import * as dotenv from "dotenv";
dotenv.config();
import { configure, getLogger } from "log4js";
configure({
    appenders: { cheese: { type: "file", filename: "cheese.log" } },
    categories: { default: { appenders: ["cheese"], level: "error" } }
  });
const logger = getLogger("cheese");

export class MongoConnection {
    uri:any = process.env.MONGO_CONNECTION || ''
    client =  require('mongodb').MongoClient;
    db:any = null;

    public ConectarMongo(callback:any){
        this.client.connect(this.uri, { useNewUrlParser: true, useUnifiedTopology: true }, (err:any, db:any) => {
            if(err){
                // logger.error("No se pudo conectar a Mongo " + err)
                console.error("No se pudo conectar a Mongo " + err)
                this.db = null
                return callback(false)
            }else{
                // logger.info("Conectado a Mongo")
                console.log("Conectado a Mongo")
                this.db = db.db()
                return callback(true)
            }

        });
    }

    public CerrarMongo(){
        this.client.close();
        logger.info("Se cerro la conexion")
    }

    public async isConnected(){
        if(this.db == null){
            return false;
        }
        try{
            await this.db.command({ ping: 1 },{$maxTimeMS:20000});
            return true;
        }catch(e){
            // logger.error("Error de conexion a mongodb " + e)
            console.log("Error de conexion " + e)
            return false;
        }
    }

    public queryMongoFind = async function(collection: string, jsonFind: any) {
        try {
            const results = await this.db.collection(collection).find(jsonFind).toArray();
            if (results == null || results.length === 0) {
                return [];
            } else {
                return results;
            }
        } catch (e) {
            console.error('Error find MongoDB: ' + e);
            return [];
        }
    };

    public queryMongoInsertSync = async function(collection: string, jsonInsert: any) {
        try {
            await this.db.collection(collection).insertOne(jsonInsert);
            return true;
        } catch (e) {
            console.error('Error find MongoDB: ' + e);
            return false;
        }
    };

    public queryMongoUpdateSync = async function(collection: string, jsonQuery: any, jsonUpdate: any) {
        try {
            await this.db.collection(collection).updateOne(jsonQuery, jsonUpdate);
            return true;
        } catch (e) {
            console.error('Error find MongoDB: ' + e);
            return false;
        }
    };
}