import * as dotenv from "dotenv";
dotenv.config();
import express from "express";
const app = express();
import bodyParser from "body-parser";
import cors from "cors";
import {MongoConnection} from "./Connection/ConexionMongo";
import dateFormat from "dateformat";
import { ServiciosUsuarios } from "./Servicios/ServiciosUsuarios";
import { ServiciosProspectos } from "./Servicios/ServiciosProspectos";

const PORT = process.env.PORT || 3002;
const mongo = new MongoConnection();
let serviciosUsuarios:any = "";
let serviciosProspectos:any = "";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
app.use(bodyParser.json());
app.use(cors());

app.get('/',(req:any, res:any)=>{
    res.send("Servidor corriendo c:");
});

app.post('/api/v1/autheticate',async (request:any, response:any) => {
    return await serviciosUsuarios.authentic(request.body.user, request.body.password, response);
});

app.post('/api/v1/insertarprospecto',async (request:any, response:any) => {
    return await serviciosProspectos.insertarprospecto(request, response);
});

app.get('/api/v1/obtenerprospecto',async (request:any, response:any) => {
    return await serviciosProspectos.obtenerprospecto(request, response);
});

app.get('/api/v1/obtenerprospectos',async (request:any, response:any) => {
    return await serviciosProspectos.obtenerprospectos(request, response);
});

app.put('/api/v1/actualizaprospecto',async (request:any, response:any) => {
    return await serviciosProspectos.actualizaprospecto(request, response);
});

app.listen(PORT,()=>{
    try{
        mongo.ConectarMongo((isConnected: boolean) => {
            if (isConnected){
                serviciosUsuarios = new ServiciosUsuarios(mongo);
                serviciosProspectos = new ServiciosProspectos(mongo);
                console.info(`Server on ${PORT}`);
            }else{
                console.info(`No pudo arrancar el Server on ${PORT}`);
            }
        })
    }catch(e){
        console.log("error isConnected " + e)
    }
});
