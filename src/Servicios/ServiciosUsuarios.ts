
import {MongoConnection} from "../Connection/ConexionMongo";
import dateFormat from "dateformat";


const mongo = new MongoConnection();
export class ServiciosUsuarios {
    private mongo:any;
    constructor(mongocon: any) {
        this.mongo = mongocon;
    }
    public async authentic(user: string, password : string, responsebody: any) : Promise<any> {
        if (await this.mongo.isConnected()){
            const User = await this.mongo.queryMongoFind('Usuarios', {user});
            if (User !== null && User.length > 0) {
                if (User[0].pass === password){
                    const token = "iQtH3x5L8kqMwVnNetIygw%3d%3d"
                    const jsonresponse = {
                        user,
                        token
                    }
                    responsebody.send(jsonresponse);
                }else{
                    responsebody.status(400).send({status:"ERROR",msg:"La contraseña es incorrecta"})
                }
            } else {
                responsebody.status(404).send({status:"ERROR",msg:"No se encontro el dato en Usuarios."})
            }
        }else{
            responsebody.status(400).send({status:"ERROR",msg:"Se perdio la conexion a Mongo"})
        }
        return responsebody;
    }
}