
import {MongoConnection} from "../Connection/ConexionMongo";
import dateFormat from "dateformat";

const mongo = new MongoConnection();

export class ServiciosProspectos {
    private mongo:any;
    constructor(mongocon: any) {
        this.mongo = mongocon;
    }
    public async insertarprospecto(request: any, response: any) : Promise<any> {
        const body = request.body;
        const headers = request.headers;
        if (!("authorization" in headers)){
            response.status(401).send({status:"ERROR",msg:"Token invalido"})
        }else{
            if (await this.mongo.isConnected()){
                if (!("nombre" in body) || !("apellidopaterno" in body) || !("calle" in body)
                || !("numerocasa" in body) || !("colonia" in body) || !("codigopostal" in body)
                || !("telefono" in body) || !("rfc" in body) || !("documentos" in body) ){
                    response.status(400).send({status:"ERROR",msg:"Parametros incorrectos"})
                }else{
                    const prospecto = await this.mongo.queryMongoFind('Usuarios', {rfc:body.rfc});
                    if (!(prospecto === null || prospecto.length === 0)) {
                        response.status(400).send({status:"ERROR",msg:"El prospecto ya fue registrado"})
                    }else{
                        const jsonInsert:any = {
                            nombre:body.nombre,
                            apellidopaterno:body.apellidopaterno,
                            apellidomaterno:body.apellidomaterno,
                            calle:body.calle,
                            numerocasa:body.numerocasa,
                            colonia:body.colonia,
                            codigopostal:body.codigopostal,
                            telefono:body.telefono,
                            rfc:body.rfc,
                            estatus:"Enviado",
                            observaciones:"",
                            documentos:[],
                            fechaRegistro: dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss.l").toString()
                        }
                        body.documentos.map((x:any, i:any) => {
                            const fs = require('fs')
                            let base64Data = x.fileBase64.replace(/^data:image\/png;base64,/, "");
                            base64Data = base64Data.replace(/^data:image\/jpeg;base64,/, "");
                            base64Data = base64Data.replace(/^data:application\/pdf;base64,/, "");
                            const rutaarchivo = 'Files/'+body.rfc+'/'
                            if (!fs.existsSync(rutaarchivo)){
                                fs.mkdirSync(rutaarchivo);
                            }
                            const nombrearchivo = x.FileNom +'.'+x.fileType
                            fs.writeFile(rutaarchivo+nombrearchivo, base64Data, 'base64', (err:any) => {
                                if(err){
                                    console.log('fs error ' + err)
                                }
                            });
                            jsonInsert.documentos.push({
                                archivo:nombrearchivo,
                                ruta:rutaarchivo+nombrearchivo
                            })
                        })
                        if (await this.mongo.queryMongoInsertSync("Prospectos",jsonInsert)){
                            response.send({status:"SUCCESS",msg:"Se guardo el registro exitosamente"})
                        }else{
                            response.status(409).send({status:"ERROR",msg:"Hubo un problema al guardar el registro"})
                        }
                    }
                }
            }else{
                response.status(400).send({status:"ERROR",msg:"Se perdio la conexion a Mongo'"})
            }
        }
        return response;
    }

    public async obtenerprospecto(request: any, response: any) : Promise<any> {
        try{
            let qrfc = ""
            if ("rfc" in request.query){
                qrfc = request.query.rfc
            }
            if (!("authorization" in request.headers)){
                response.status(401).send({status:"ERROR",msg:"Token invalido"})
            }else{
                if (await this.mongo.isConnected()){
                    const prospectos = await this.mongo.queryMongoFind('Prospectos', {rfc:qrfc});
                    if (!(prospectos === null || prospectos.length === 0)) {
                        const fs = require('fs')
                        const documentosbase64:any = []
                        prospectos[0].documentos.map((x:any, i:any) => {
                            const dataFile = fs.readFileSync(x.ruta).toString('base64')
                            documentosbase64.push({
                                fileData: dataFile,
                                fileNom: x.archivo
                            })
                        })
                        prospectos[0].documentos = documentosbase64
                        response.send({status:"SUCCESS", msg:"Exito", data:prospectos})
                    }else{
                        response.status(400).send({status:"ERROR", msg:"No se encontraron prospectos", data:[]})
                    }
                }else{
                    response.status(400).send({status:"ERROR", msg:"Se perdio la conexion a Mongo", data:[]})
                }
            }
        }catch(e){
            response.status(500).send({status:"ERROR", msg:e, data:[]})
        }
        return response;
    }

    public async obtenerprospectos(request: any, response: any) : Promise<any> {
        if (!("authorization" in request.headers)){
            response.status(401).send({status:"ERROR",msg:"Token invalido"})
        }else{
            if (await this.mongo.isConnected()){
                const prospectos = await this.mongo.queryMongoFind('Prospectos', {});
                if (!(prospectos === null || prospectos.length === 0)) {
                    const jsonresponse:any = []
                    prospectos.map((x:any, i:any) => {
                        const prospecto = {
                            _id: x._id,
                            nombre: x.nombre,
                            apellidopaterno: x.apellidopaterno,
                            apellidomaterno: x.apellidomaterno,
                            estatus: x.estatus,
                            rfc: x.rfc
                        }
                        jsonresponse.push(prospecto)
                    })
                    response.send({status:"SUCCESS", msg:"Exito", data:jsonresponse})
                }else{
                    response.status(400).send({status:"ERROR", msg:"No se encontraron prospectos", data:[]})
                }
            }else{
                response.status(400).send({status:"ERROR", msg:"Se perdio la conexion a Mongo", data:[]})
            }
        }
        return response;
    }
    public async actualizaprospecto(request: any, response: any) : Promise<any> {
        if (!("authorization" in request.headers)){
            response.status(401).send({status:"ERROR",msg:"Token invalido"})
        }else{
            if (await this.mongo.isConnected()){

                const prospectos = await this.mongo.queryMongoUpdateSync('Prospectos', {rfc:request.body.rfc}, {$set: {estatus:request.body.estatus, observaciones:request.body.observaciones}});
                if (prospectos) {
                    response.send({status:"SUCCESS", msg:"Exito", data:[]})
                }else{
                    response.status(409).send({status:"ERROR", msg:"Hubo un problema al actualizar", data:[]})
                }
            }else{
                response.status(400).send({status:"ERROR", msg:"Se perdio la conexion a Mongo", data:[]})
            }
        }
        return response;
    }
}