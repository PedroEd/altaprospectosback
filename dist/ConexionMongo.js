"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoConnection = void 0;
const dotenv = __importStar(require("dotenv"));
dotenv.config();
const log4js_1 = require("log4js");
log4js_1.configure({
    appenders: { cheese: { type: "file", filename: "cheese.log" } },
    categories: { default: { appenders: ["cheese"], level: "error" } }
});
const logger = log4js_1.getLogger("cheese");
class MongoConnection {
    constructor() {
        this.uri = process.env.MONGO_CONNECTION || '';
        this.client = require('mongodb').MongoClient;
        this.db = null;
        this.queryMongoFind = function (collection, jsonFind) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const results = yield this.db.collection(collection).find(jsonFind).toArray();
                    if (results == null || results.length === 0) {
                        return [];
                    }
                    else {
                        return results;
                    }
                }
                catch (e) {
                    console.error('Error find MongoDB: ' + e);
                    return [];
                }
            });
        };
        this.queryMongoInsertSync = function (collection, jsonInsert) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    yield this.db.collection(collection).insertOne(jsonInsert);
                    return true;
                }
                catch (e) {
                    console.error('Error find MongoDB: ' + e);
                    return false;
                }
            });
        };
        this.queryMongoUpdateSync = function (collection, jsonQuery, jsonUpdate) {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    yield this.db.collection(collection).updateOne(jsonQuery, jsonUpdate);
                    return true;
                }
                catch (e) {
                    console.error('Error find MongoDB: ' + e);
                    return false;
                }
            });
        };
    }
    ConectarMongo(callback) {
        this.client.connect(this.uri, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
            if (err) {
                // logger.error("No se pudo conectar a Mongo " + err)
                console.error("No se pudo conectar a Mongo " + err);
                this.db = null;
                return callback(false);
            }
            else {
                // logger.info("Conectado a Mongo")
                console.log("Conectado a Mongo");
                this.db = db.db();
                return callback(true);
            }
        });
    }
    CerrarMongo() {
        this.client.close();
        logger.info("Se cerro la conexion");
    }
    isConnected() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.db == null) {
                return false;
            }
            try {
                yield this.db.command({ ping: 1 }, { $maxTimeMS: 20000 });
                return true;
            }
            catch (e) {
                // logger.error("Error de conexion a mongodb " + e)
                console.log("Error de conexion " + e);
                return false;
            }
        });
    }
}
exports.MongoConnection = MongoConnection;
//# sourceMappingURL=ConexionMongo.js.map