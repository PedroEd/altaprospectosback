"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiciosUsuarios = void 0;
const ConexionMongo_1 = require("../Connection/ConexionMongo");
const mongo = new ConexionMongo_1.MongoConnection();
class ServiciosUsuarios {
    constructor(mongocon) {
        this.mongo = mongocon;
    }
    authentic(user, password, responsebody) {
        return __awaiter(this, void 0, void 0, function* () {
            if (yield this.mongo.isConnected()) {
                const User = yield this.mongo.queryMongoFind('Usuarios', { user });
                if (User !== null && User.length > 0) {
                    if (User[0].pass === password) {
                        const token = "iQtH3x5L8kqMwVnNetIygw%3d%3d";
                        const jsonresponse = {
                            user,
                            token
                        };
                        responsebody.send(jsonresponse);
                    }
                    else {
                        responsebody.status(400).send({ status: "ERROR", msg: "La contraseña es incorrecta" });
                    }
                }
                else {
                    responsebody.status(404).send({ status: "ERROR", msg: "No se encontro el dato en Usuarios." });
                }
            }
            else {
                responsebody.status(400).send({ status: "ERROR", msg: "Se perdio la conexion a Mongo" });
            }
            return responsebody;
        });
    }
}
exports.ServiciosUsuarios = ServiciosUsuarios;
//# sourceMappingURL=ServiciosUsuarios.js.map