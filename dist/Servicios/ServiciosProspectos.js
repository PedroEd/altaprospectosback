"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiciosProspectos = void 0;
const ConexionMongo_1 = require("../Connection/ConexionMongo");
const dateformat_1 = __importDefault(require("dateformat"));
const mongo = new ConexionMongo_1.MongoConnection();
class ServiciosProspectos {
    constructor(mongocon) {
        this.mongo = mongocon;
    }
    insertarprospecto(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            const body = request.body;
            const headers = request.headers;
            if (!("authorization" in headers)) {
                response.status(401).send({ status: "ERROR", msg: "Token invalido" });
            }
            else {
                if (yield this.mongo.isConnected()) {
                    if (!("nombre" in body) || !("apellidopaterno" in body) || !("calle" in body)
                        || !("numerocasa" in body) || !("colonia" in body) || !("codigopostal" in body)
                        || !("telefono" in body) || !("rfc" in body) || !("documentos" in body)) {
                        response.status(400).send({ status: "ERROR", msg: "Parametros incorrectos" });
                    }
                    else {
                        const prospecto = yield this.mongo.queryMongoFind('Usuarios', { rfc: body.rfc });
                        if (!(prospecto === null || prospecto.length === 0)) {
                            response.status(400).send({ status: "ERROR", msg: "El prospecto ya fue registrado" });
                        }
                        else {
                            const jsonInsert = {
                                nombre: body.nombre,
                                apellidopaterno: body.apellidopaterno,
                                apellidomaterno: body.apellidomaterno,
                                calle: body.calle,
                                numerocasa: body.numerocasa,
                                colonia: body.colonia,
                                codigopostal: body.codigopostal,
                                telefono: body.telefono,
                                rfc: body.rfc,
                                estatus: "Enviado",
                                observaciones: "",
                                documentos: [],
                                fechaRegistro: dateformat_1.default(new Date(), "yyyy-mm-dd HH:MM:ss.l").toString()
                            };
                            body.documentos.map((x, i) => {
                                const fs = require('fs');
                                let base64Data = x.fileBase64.replace(/^data:image\/png;base64,/, "");
                                base64Data = base64Data.replace(/^data:image\/jpeg;base64,/, "");
                                base64Data = base64Data.replace(/^data:application\/pdf;base64,/, "");
                                const rutaarchivo = 'Files/' + body.rfc + '/';
                                if (!fs.existsSync(rutaarchivo)) {
                                    fs.mkdirSync(rutaarchivo);
                                }
                                const nombrearchivo = x.FileNom + '.' + x.fileType;
                                fs.writeFile(rutaarchivo + nombrearchivo, base64Data, 'base64', (err) => {
                                    if (err) {
                                        console.log('fs error ' + err);
                                    }
                                });
                                jsonInsert.documentos.push({
                                    archivo: nombrearchivo,
                                    ruta: rutaarchivo + nombrearchivo
                                });
                            });
                            if (yield this.mongo.queryMongoInsertSync("Prospectos", jsonInsert)) {
                                response.send({ status: "SUCCESS", msg: "Se guardo el registro exitosamente" });
                            }
                            else {
                                response.status(409).send({ status: "ERROR", msg: "Hubo un problema al guardar el registro" });
                            }
                        }
                    }
                }
                else {
                    response.status(400).send({ status: "ERROR", msg: "Se perdio la conexion a Mongo'" });
                }
            }
            return response;
        });
    }
    obtenerprospecto(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let qrfc = "";
                if ("rfc" in request.query) {
                    qrfc = request.query.rfc;
                }
                if (!("authorization" in request.headers)) {
                    response.status(401).send({ status: "ERROR", msg: "Token invalido" });
                }
                else {
                    if (yield this.mongo.isConnected()) {
                        const prospectos = yield this.mongo.queryMongoFind('Prospectos', { rfc: qrfc });
                        if (!(prospectos === null || prospectos.length === 0)) {
                            const fs = require('fs');
                            const documentosbase64 = [];
                            prospectos[0].documentos.map((x, i) => {
                                const dataFile = fs.readFileSync(x.ruta).toString('base64');
                                documentosbase64.push({
                                    fileData: dataFile,
                                    fileNom: x.archivo
                                });
                            });
                            prospectos[0].documentos = documentosbase64;
                            response.send({ status: "SUCCESS", msg: "Exito", data: prospectos });
                        }
                        else {
                            response.status(400).send({ status: "ERROR", msg: "No se encontraron prospectos", data: [] });
                        }
                    }
                    else {
                        response.status(400).send({ status: "ERROR", msg: "Se perdio la conexion a Mongo", data: [] });
                    }
                }
            }
            catch (e) {
                response.status(500).send({ status: "ERROR", msg: e, data: [] });
            }
            return response;
        });
    }
    obtenerprospectos(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!("authorization" in request.headers)) {
                response.status(401).send({ status: "ERROR", msg: "Token invalido" });
            }
            else {
                if (yield this.mongo.isConnected()) {
                    const prospectos = yield this.mongo.queryMongoFind('Prospectos', {});
                    if (!(prospectos === null || prospectos.length === 0)) {
                        const jsonresponse = [];
                        prospectos.map((x, i) => {
                            const prospecto = {
                                _id: x._id,
                                nombre: x.nombre,
                                apellidopaterno: x.apellidopaterno,
                                apellidomaterno: x.apellidomaterno,
                                estatus: x.estatus,
                                rfc: x.rfc
                            };
                            jsonresponse.push(prospecto);
                        });
                        response.send({ status: "SUCCESS", msg: "Exito", data: jsonresponse });
                    }
                    else {
                        response.status(400).send({ status: "ERROR", msg: "No se encontraron prospectos", data: [] });
                    }
                }
                else {
                    response.status(400).send({ status: "ERROR", msg: "Se perdio la conexion a Mongo", data: [] });
                }
            }
            return response;
        });
    }
    actualizaprospecto(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!("authorization" in request.headers)) {
                response.status(401).send({ status: "ERROR", msg: "Token invalido" });
            }
            else {
                if (yield this.mongo.isConnected()) {
                    const prospectos = yield this.mongo.queryMongoUpdateSync('Prospectos', { rfc: request.body.rfc }, { $set: { estatus: request.body.estatus, observaciones: request.body.observaciones } });
                    if (prospectos) {
                        response.send({ status: "SUCCESS", msg: "Exito", data: [] });
                    }
                    else {
                        response.status(409).send({ status: "ERROR", msg: "Hubo un problema al actualizar", data: [] });
                    }
                }
                else {
                    response.status(400).send({ status: "ERROR", msg: "Se perdio la conexion a Mongo", data: [] });
                }
            }
            return response;
        });
    }
}
exports.ServiciosProspectos = ServiciosProspectos;
//# sourceMappingURL=ServiciosProspectos.js.map