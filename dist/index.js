"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
dotenv.config();
const express_1 = __importDefault(require("express"));
const app = express_1.default();
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const ConexionMongo_1 = require("./Connection/ConexionMongo");
const ServiciosUsuarios_1 = require("./Servicios/ServiciosUsuarios");
const ServiciosProspectos_1 = require("./Servicios/ServiciosProspectos");
const PORT = process.env.PORT || 3002;
const mongo = new ConexionMongo_1.MongoConnection();
let serviciosUsuarios = "";
let serviciosProspectos = "";
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(body_parser_1.default.json({ limit: '10mb' }));
app.use(body_parser_1.default.urlencoded({ limit: '10mb', extended: true }));
app.use(body_parser_1.default.json());
app.use(cors_1.default());
app.get('/', (req, res) => {
    res.send("Servidor corriendo c:");
});
app.post('/api/v1/autheticate', (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    return yield serviciosUsuarios.authentic(request.body.user, request.body.password, response);
}));
app.post('/api/v1/insertarprospecto', (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    return yield serviciosProspectos.insertarprospecto(request, response);
}));
app.get('/api/v1/obtenerprospecto', (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    return yield serviciosProspectos.obtenerprospecto(request, response);
}));
app.get('/api/v1/obtenerprospectos', (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    return yield serviciosProspectos.obtenerprospectos(request, response);
}));
app.put('/api/v1/actualizaprospecto', (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    return yield serviciosProspectos.actualizaprospecto(request, response);
}));
app.listen(PORT, () => {
    try {
        mongo.ConectarMongo((isConnected) => {
            if (isConnected) {
                serviciosUsuarios = new ServiciosUsuarios_1.ServiciosUsuarios(mongo);
                serviciosProspectos = new ServiciosProspectos_1.ServiciosProspectos(mongo);
                console.info(`Server on ${PORT}`);
            }
            else {
                console.info(`No pudo arrancar el Server on ${PORT}`);
            }
        });
    }
    catch (e) {
        console.log("error isConnected " + e);
    }
});
//# sourceMappingURL=index.js.map