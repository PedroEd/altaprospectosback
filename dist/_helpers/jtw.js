"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Jwt = void 0;
const express_jwt_1 = __importDefault(require("express-jwt"));
class Jwt {
    jwt() {
        const { secret } = { secret: process.env.SECRET };
        return express_jwt_1.default({ secret, algorithms: ['HS256'] }).unless({
            path: [
                // public routes that don't require authentication
                '/api/v1/autheticate'
            ]
        });
    }
}
exports.Jwt = Jwt;
//# sourceMappingURL=jtw.js.map